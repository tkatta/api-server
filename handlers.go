package main

import (
	"encoding/json"
	"net/http"
)

var user User

func pingHandlerFunc(w http.ResponseWriter, r *http.Request){
	if r.Method != http.MethodGet{
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Malformed Request"))
	}
	resp := getServerHealth()
	body , err := json.MarshalIndent(resp,"", "")
	if err != nil{
		panic(err)
	}
	w.Write(body)
	w.WriteHeader(http.StatusOK)
}

func userHandlerFunc (w http.ResponseWriter, r *http.Request){
	if r.Method == http.MethodGet{
		resp:= user.getUserDetails()
		body , err := json.MarshalIndent(resp,"","")
		if err != nil{
			panic(err)
		}
		w.Write(body)
		w.WriteHeader(http.StatusOK)
	}
}
