package main

import (
	//"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPingHandlerFunc(t *testing.T){
	assert := assert.New(t)
	req, err := http.NewRequest(http.MethodGet, "/ping", nil)
	if err != nil{
		t.Error(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(pingHandlerFunc)
	handler.ServeHTTP(rr, req)
	assert.Equal(http.StatusOK, rr.Code)
}
