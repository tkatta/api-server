package main

import "time"

type Health struct{
	Status string `json:"status"`
	Version string `json:"version"`
	Time time.Time
}

type User struct{
	Id string `json:"id"`
	UserName string `json:"username"`
	Email string `json:"email"`
}