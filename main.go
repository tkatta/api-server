package main

import (
	"log/slog"
	"net/http"
)

func main(){
	server := http.NewServeMux()
	slog.Info("Started API server on localhost:8085")
	server.HandleFunc("GET /ping", pingHandlerFunc)
	server.HandleFunc("GET /user/all", userHandlerFunc)
	err := http.ListenAndServe(":8085", server)
	if err != nil{
		panic(err)
	}
}
