package main

import "time"

func getServerHealth() Health{
	ping := Health{
		Status: "healthy",
		Version: "v0.0.1",
		Time: time.Now(),
	}
	return ping
}

func (u *User) getUserDetails() *User{
	return &User{
		Id: u.Id,
		UserName: u.UserName,
		Email: u.Email,
	}
}