package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetServerHealth(t *testing.T){
	ass := assert.New(t)
	expStatus:= "healthy"
	actualPing := getServerHealth()
   	ass.Equal(expStatus, actualPing.Status)
}

func TestGetUserDetails(t *testing.T){
	ass := assert.New(t)
	testUser := User{
		Id: "379",
		UserName: "tkatta",
	}
	expUser := "tkatta"
	actualUser := testUser.getUserDetails()
	ass.Equal(expUser, actualUser.UserName)

}